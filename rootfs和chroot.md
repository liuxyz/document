# rootfs

rootfs是linux中重要的一个概念，在官方的文档[ramfs, rootfs and initramfs](https://www.kernel.org/doc/Documentation/filesystems/ramfs-rootfs-initramfs.txt)中是将rootfs和内存文件系统一起讲的
> rootfs is a special instance of ramfs (or tmpfs, if that's enabled), which is always present in 2.6 systems.  You can't unmount rootfs for approximately the same reason you can't kill the init process; rather than having special code to check for and handle an empty list, it's smaller and simpler for the kernel to just make sure certain lists can't become empty.


ramfs是一种内存系统，而rootfs是ramfs一种实现，系统一启动的时候就是吧rootfs挂载到`/`根目录所以叫根文件系统rootfs，这个目录是个空的不能被解挂的系统，然后大多数系统只是在 rootfs 上挂载另一个文件系统并忽略它。

所以一般rootfs都是指的后来的覆盖`/`根目录的文件，IBM AIX的文档中是这样描述的
> The root file system is the top of the hierarchical file tree. It contains the files and directories critical for system operation, including the device directory and programs for booting the system. The root file system also contains mount points where file systems can be mounted to connect to the root file system hierarchy.

根文件系统是分层文件树的顶部。它包含对系统操作至关重要的文件和目录，包括设备目录和启动系统的程序。根文件系统还包含可以安装文件系统以连接到根文件系统层次结构的安装点。

所以这里简单的总结下**通常所说的rootfs是指的一些文件和目录，他是被挂载linux系统的根目录下的**

ok，到这里可以明确一点rootfs中到底包含什么呢，通常一些linux发行版都会提供rootfs，我们通过两个比较常用的发行版Ubuntu和alpine的rootfs观察
* [Ubuntu 21.04 rootfs](http://cdimage.ubuntu.com/ubuntu-base/releases/21.04/release/)
* [alpine v3.14 rootfs](https://dl-cdn.alpinelinux.org/alpine/v3.14/releases/x86_64/)

ubuntu-base-21.04-base-amd64：
```bash
liu@liu:~$ tree ubunt-rootfs/ -L 1
ubunt-rootfs/
├── alpine
├── bin -> usr/bin
├── boot
├── dev
├── etc
├── home
├── lib -> usr/lib
├── lib32 -> usr/lib32
├── lib64 -> usr/lib64
├── libx32 -> usr/libx32
├── media
├── mnt
├── opt
├── proc
├── root
├── run
├── sbin -> usr/sbin
├── srv
├── sys
├── tmp
├── ubuntu-base-21.04-base-amd64.tar.gz
├── usr
└── var
```
alpine-minirootfs-3.14.1-x86_64：
```bash
liu@liu:~$ tree -L 1 alpine-rootfs/
alpine-rootfs/
├── alpine-minirootfs-3.14.1-x86_64.tar.gz
├── bin
├── dev
├── etc
├── home
├── lib
├── media
├── mnt
├── opt
├── proc
├── root
├── run
├── sbin
├── srv
├── sys
├── tmp
├── usr
└── var
```
Ubuntu 20.04.2 LTS：
```bash
liu@liu:~$ tree -L 1 /
/
├── bin -> usr/bin
├── boot
├── cdrom
├── dev
├── etc
├── home
├── lib -> usr/lib
├── lib32 -> usr/lib32
├── lib64 -> usr/lib64
├── libx32 -> usr/libx32
├── lost+found
├── media
├── mnt
├── opt
├── proc
├── redis-data
├── root
├── run
├── sbin -> usr/sbin
├── snap
├── srv
├── swap.img
├── sys
├── tmp
├── usr
└── var
```

通过对比可以发现rootfs基本包含以下的部分：

| Item | Description |
| ---  | ----------- |
| /etc  | 该目录下存放着各种配置文件，对于PC上的Linux系统，/etc目录下的文件和目录非常多，这些目录文件是可选的，它们依赖于系统中所拥有的应用程序，依赖于这些程序是否需要配置文件。在嵌入式系统中，这些内容可以大为精减。 |
| /bin  | 该目录下存放所有用户都可以使用的、基本的命令，这些命令在挂接其它文件系统之前就可以使用，所以/bin目录必须和根文件系统在同一个分区中。 |
| /sbin | 该目录下存放系统命令，即只有管理员能够使用的命令，系统命令还可以存放在/usr/sbin,/usr/local/sbin目录下，/sbin目录中存放的是基本的系统命令，它们用于启动系统，修复系统等，与/bin目录相似，在挂接其他文件系统之前就可以使用/sbin，所以/sbin目录必须和根文件系统在同一个分区中。|
| /lib  | 该目录下存放共享库和可加载(驱动程序)，共享库用于启动系统。运行根文件系统中的可执行程序，比如：/bin /sbin 目录下的程序。|
| /dev  | 该目录下存放的是设备文件，设备文件是Linux中特有的文件类型，在Linux系统下，以文件的方式访问各种设备，即通过读写某个设备文件操作某个具体硬件。|
| /home | 用户目录，它是可选的，对于每个普通用户，在/home目录下都有一个以用户名命名的子目录，里面存放用户相关的配置文件。|
| /root | 根用户的目录，与此对应，普通用户的目录是/home下的某个子目录。|
| /usr  | /usr目录的内容可以存在另一个分区中，在系统启动后再挂接到根文件系统中的/usr目录下。里面存放的是共享、只读的程序和数据，这表明/usr目录下的内容可以在多个主机间共享，这些主要也符合FHS标准的。/usr中的文件应该是只读的，其他主机相关的，可变的文件应该保存在其他目录下，比如/var。/usr目录在嵌入式中可以精减。|
| /var  | 与/usr目录相反，/var目录中存放可变的数据，比如spool目录(mail,news)，log文件，临时文件。|
| /proc | 这是一个空目录，常作为proc文件系统的挂接点，proc文件系统是个虚拟的文件系统，它没有实际的存储设备，里面的目录，文件都是由内核临时生成的，用来表示系统的运行状态，也可以操作其中的文件控制系统。|
| /mnt  | 用于临时挂载某个文件系统的挂接点，通常是空目录，也可以在里面创建一引起空的子目录，比如/mnt/cdram /mnt/hda1 。用来临时挂载光盘、硬盘。|
| /tmp  | 用于存放临时文件，通常是空目录，一些需要生成临时文件的程序用到的/tmp目录下，所以/tmp目录必须存在并可以访问。|

rootfs被挂载了`/`根目录，且包含一个系统的基础命令工具。

其实关于文件目录结构的定义是有一个标准叫FHS（Filesystem Hierarchy Standard）, FHS定义了linux发行版的目录层级结构,于94年发布了第一个1.0版本,目前最新的版本是15年发布的3.0版本。
有关FHS的详细介绍可以参看维基百科[Filesystem Hierarchy Standard](https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard)。

# chroot
上面rootfs部分介绍过，rootfs是被挂载到根目录且包含了基础的命令行工具，那么我在宿主机中下载解压出来了一个rootfs，如果能将rootfs所在的目录挂载到`/`根目录中去不就相当于切换了系统的所有的基础工具。

例如我在Ubuntu中使用的是apt包管理器，如果我将alpine-rootfs挂载到`/`根目录，就可以使用apk包管理器。
但是直接挂载的话会有个问题，宿主机运行的内容就在当前的rootfs上，如果被替换会造成当前系统的崩溃。那么就只能重启系统在系统启动时将alpine挂载至`/`根目录。这种通过重启来更换rootfs环境的方式势必造成很大的负担。
所以linux中提供了chroot命令实现该功能。

chroot是起源于Unix系统的一个操作，作用于正在运行的进程和它的子进程，改变它外显的根目录。一个运行在这个环境下，经由chroot设置根目录的程序，它不能够对这个指定根目录之外的文件进行访问动作，不能读取，也不能更改它的内容。
chroot提供了以下的特性:

* 测试和开发
    可以经由chroot创建一个测试环境，用来测试软件。这可以减少将软件直接布署到整个生产系统中可能造成的风险。
* 依赖控制
    可以在chroot创建的环境下，进行软件开发，组建以及测试，只保留这个程序需要的软件依赖。这可以避免在系统中预先安装的各种软件库，影响到开发，造成软件开发者在组建软件时，可能遇到一些链接冲突。
* 兼容性
    早期遗留软件或使用不同应用二进制接口（ABI）的软件，因为它们提供的软件库和宿主机的库之间，可能发生名称或链接冲突，可以在chroot环境下运行，以保持系统安全。
* 修复
    当一个系统不能启动时，可以使用chroot，先从另一个根文件系统（比如从安装媒体，或是Live CD）下引导，之后再回到受损的环境中，重新修正系统。
* 特权分离
    将允许开启文件描述符（例如文件，流水线或是网络连线）的程序放到chroot下运行，不用特地将工作所需的文件，放到chroot路径底下，这可以简化软件监狱的设计。chroot简化了安全设计，可以创造出一个沙盒环境，来运行一个有潜在危险的特权程序，以先期防御可能的安全漏洞。但值得注意的是，chroot没有足够能力去防御一个拥有root特权的行程造成危害。

通过chroot能够使用rootfs创建一个新的环境处理来，这个新的环境和宿主机是相互隔离的。因为chroot最早是被用于软件开发所以chroot所创建出来的根目录,也被称作软件监狱（chroot jail）。后面黑客又发展出通过chroot获取宿主机权限的技术被称为越狱（jailbreak）。以及linux发展处的控制组群（cgroups）来进行资源限制。相关资料可以查看维基百科：
* [Chroot](https://en.wikipedia.org/wiki/Chroot)
* [Cgroups](https://en.wikipedia.org/wiki/Cgroups)

例如Ubuntu和alpine使用的是不同的libc库Ubuntu使用的glibc和alpine使用的musl libc在实际应用中会有各种差异，就可以通过chroot来创建不同的依赖环境。或者也可以通过chroot来切换发行版的不同版本。
[![asciicast](https://asciinema.org/a/fCtrEpsNGYuu3gbtODPfDHsSc.svg)](https://asciinema.org/a/fCtrEpsNGYuu3gbtODPfDHsSc)

# rootfs 和 chroot 的价值
rootfs和chroot的使用提供了一种区别于虚拟机的更为廉价的环境构建与隔离手段，现在很火的容器技术其本质也是通过chroot进行隔离配合cgroups资源限制等技术实现。
